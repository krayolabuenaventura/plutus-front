'use strict';

/**
 * @ngdoc function
 * @name plutusFrontApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the plutusFrontApp
 */
angular.module('plutusFrontApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
