'use strict';

/**
 * @ngdoc function
 * @name plutusFrontApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the plutusFrontApp
 */
angular.module('plutusFrontApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
